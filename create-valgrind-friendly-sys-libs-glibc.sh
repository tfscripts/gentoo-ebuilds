#!/usr/bin/env bash

workingdir="${PWD}"
test -n "$1" && workingdir="$1"
pushd "${workingdir}" >/dev/null || { echo "Cannot change into working directory '${workingdir}'" >&2 ; exit 1 ; }

test "${PWD}" = "/usr/portage" && { popd ; echo "For security reasons, this script may not operate in '/usr/portage'" >&2  ; exit 1 ; }

test -d sys-libs || { popd ; echo "To ensure operating on the right directory, there must be an existing 'sys-libs' directory" >&2 ; exit 1 ; }

rm -rf sys-libs/glibc
mkdir sys-libs/glibc || { popd ; echo "Cannot create directory 'sys-libs/glibc'" >&2 ; exit 1 ; }

rsync -a /usr/portage/sys-libs/glibc/files sys-libs/glibc/ || { popd ; echo "Syncing files for 'sys-libs/glibc' from official portage tree failed" >&2 ; exit 1 ; }
sed -i -e 's/append-flags -O2 -fno-strict-aliasing/append-flags -O2 -fno-strict-aliasing -fno-builtin-strlen/' sys-libs/glibc/files/eblits/common.eblit || { popd ; echo "Changing flags in 'files/eblits/common.eblit' failed" >&2 ; exit 1 ; }
test "$(md5sum </usr/portage/sys-libs/glibc/files/eblits/common.eblit)" = "$(md5sum <sys-libs/glibc/files/eblits/common.eblit)" && { popd ; echo "Patch operation did not change 'files/eblits/common.eblit'" >&2 ; exit 1 ; }

lateststatbleebuild=$(grep --color=NEVER -l -E '[^~]amd64' /usr/portage/sys-libs/glibc/glibc-2*.ebuild | tail -n 1)
test -z "${lateststatbleebuild}" && { popd ; echo "Cannot determine latest stable glibc ebuild" >&2 ; exit 1 ; }
revision=$(echo "${lateststatbleebuild}" | grep -o -E --color=NEVER '[-]r[0-9][0-9]*[.]ebuild$' | sed -e 's/^-r//;s/.ebuild$//')
test -z "${revision}" && revision=0
(( ++revision ))
targetebuild=$(echo "${lateststatbleebuild/\/usr\/portage\//}" | sed -e 's/\(-r[0-9][0-9]*\)*[.]ebuild/-r'${revision}'.ebuild/')
cp "${lateststatbleebuild}" "${targetebuild}"

pushd sys-libs/glibc >/dev/null
sudo ebuild glibc-2*.ebuild digest || { popd ; popd ; echo "Cannot build Manifest file" >&2 ; exit 1 ; }
popd >/dev/null

popd >/dev/null
