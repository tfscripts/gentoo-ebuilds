# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit eutils font git-r3

EGIT_REPO_URI="https://github.com/chrissimpkins/codeface.git"

DESCRIPTION="Typefaces for source code beautification"
HOMEPAGE="https://github.com/chrissimpkins/codeface/blob/master/README.md"

LICENSE="CC-BY-4.0"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~ia64 ~ppc ~ppc64 ~sh ~sparc x86 ~x86-fbsd"
IUSE="X"

RDEPEND="media-libs/fontconfig"
DEPEND="sys-apps/findutils"

src_install() {
	insinto ${FONTDIR}
	find "${S}/fonts" -type f -iname '*.otf' | while read fontfilename ; do doins "$fontfilename" || die "Could not install '$fontfilename'" ; done
	find "${S}/fonts" -type f -iname '*.ttf' | while read fontfilename ; do doins "$fontfilename" || die "Could not install '$fontfilename'" ; done

	use X && font_xfont_config
	font_fontconfig

	# TODO documentation
}
