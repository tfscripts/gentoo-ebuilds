# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils font

DESCRIPTION="Cooper Hewitt: the typeface by Chester Jenkins"
HOMEPAGE="https://www.cooperhewitt.org/open-source-at-cooper-hewitt/cooper-hewitt-the-typeface-by-chester-jenkins/"
SRC_URI="https://www.cooperhewitt.org/wp-content/uploads/fonts/CooperHewitt-OTF-public.zip"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~ia64 ~ppc ~ppc64 ~sh ~sparc x86 ~x86-fbsd"

RDEPEND="media-libs/fontconfig"

S="${WORKDIR}/CooperHewitt-OTF-public"

src_install() {
	insinto ${FONTDIR}
	for otffile in CooperHewitt-BoldItalic.otf CooperHewitt-Bold.otf CooperHewitt-BookItalic.otf \
	              CooperHewitt-Book.otf CooperHewitt-HeavyItalic.otf CooperHewitt-Heavy.otf \
	              CooperHewitt-LightItalic.otf CooperHewitt-Light.otf CooperHewitt-MediumItalic.otf \
	              CooperHewitt-Medium.otf CooperHewitt-SemiboldItalic.otf CooperHewitt-Semibold.otf \
	              CooperHewitt-ThinItalic.otf CooperHewitt-Thin.otf ; do
		doins "${otffile}" || die "Could not install '${otffile}'"
	done

	use X && font_xfont_config
	font_fontconfig

	# TODO documentation
}

