# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=6

inherit eutils font git-r3

EGIT_REPO_URI="https://git.polarsys.org/gitroot/b612/b612.git"
[ ${PV} = 1.003 ] && EGIT_COMMIT=bd14fde2544566e620eab106eb8d6f2b7fb1347e

DESCRIPTION="The PolarSys Font"
HOMEPAGE="https://www.polarsys.org/projects/polarsys.b612"

LICENSE="EPL-1.0 EDL-1.0"
SLOT="0"
if [ ${PV} = 9999 ] ; then
	KEYWORDS=""
else
	KEYWORDS="~alpha amd64 ~arm ~ia64 ~ppc ~ppc64 ~sh ~sparc x86 ~x86-fbsd"
fi

IUSE="X"

RDEPEND="media-libs/fontconfig"
DEPEND=""

src_compile() {
	einfo "Skipping building font, using TrueType files as provided in the Git repository"
}

src_install() {
	insinto ${FONTDIR}
	find -type f -name '*B612*.ttf' | while read fontfilename ; do
		doins "${fontfilename}" || die "Could not install '${fontfilename}'"
	done

	use X && font_xfont_config
	font_fontconfig
}
