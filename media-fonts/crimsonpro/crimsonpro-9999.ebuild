# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit font git-r3

EGIT_REPO_URI="https://github.com/Fonthausen/CrimsonPro.git"

DESCRIPTION="CrimsonPro is a fresh take on the original Crimson Text"
HOMEPAGE="https://github.com/Fonthausen/CrimsonPro"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="media-libs/fontconfig"

FONT_S="fonts/ttfs"
FONT_SUFFIX="ttf"

src_install() {
	font_src_install
	dodoc CONTRIBUTORS.txt AUTHORS.txt
}
