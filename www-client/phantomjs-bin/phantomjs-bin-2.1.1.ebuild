# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Scriptable Headless Browser"
HOMEPAGE="https://phantomjs.org/"
SRC_URI="https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-${PV}-linux-x86_64.tar.bz2 -> ${P}.tar.bz2"

S="${WORKDIR}/phantomjs-${PV}-linux-x86_64/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

src_install() {
	exeinto /opt/${PN}
	cd bin
	doexe phantomjs
	dosym ${EPREFIX}/opt/${PN}/phantomjs /usr/bin/phantomjs
}
