# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit eutils

DESCRIPTION=" WOFF utilities with Zopfli compression"
HOMEPAGE="https://github.com/bramstein/sfnt2woff-zopfli"
LICENSE="Apache-2.0 MPL-1.1"

if [[ ${PV} = 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/bramstein/sfnt2woff-zopfli.git"
	KEYWORDS=""
else
	SRC_URI="https://github.com/bramstein/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~x86 ~amd64"
fi

SLOT="0"

src_install() {
	dobin woff2sfnt-zopfli
	dobin sfnt2woff-zopfli
}
