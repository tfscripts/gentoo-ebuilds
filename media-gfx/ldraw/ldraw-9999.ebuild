# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="LDraw parts library"
HOMEPAGE="https://www.ldraw.org"
SRC_URI="https://www.ldraw.org/library/updates/complete.zip -> ${PN}-complete.zip"

LICENSE="CC-BY-2.0 OPL"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"

S="${WORKDIR}"

src_unpack() {
	true
}

src_install() {
	insinto /usr/share/${PN}
	newins "${DISTDIR}/${PN}-complete.zip" ${PN}.zip
}
