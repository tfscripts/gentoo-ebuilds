# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

inherit eutils git-r3 qmake-utils

# Short one-line description of this package.
DESCRIPTION="A simple Qt5-based image viewer"
HOMEPAGE="https://gitlab.com/qdisplay/qdisplay5"
EGIT_REPO_URI="https://gitlab.com/qdisplay/qdisplay5.git"

LICENSE="GPL-3"

SLOT="0"

KEYWORDS="~amd64 ~x86"

DEPEND="dev-qt/qtwidgets:5"

# Run-time dependencies. Must be defined to whatever this depends on to run.
# The below is valid if the same run-time depends are required to compile.
RDEPEND="${DEPEND}
        media-gfx/exiv2
        || ( media-gfx/imagemagick media-gfx/graphicsmagick[imagemagick] )
        virtual/jpeg:0
        media-gfx/jpegoptim
        || ( media-gfx/optipng media-gfx/pngcrush )"

# The following src_configure function is implemented as default by portage, so
# you only need to call it if you need a different behaviour.
# This function is available only in EAPI 2 and later.
src_configure() {
	eqmake5 ${PN}.pro
}

src_install() {
	emake INSTALL_ROOT="${D}" install
}
