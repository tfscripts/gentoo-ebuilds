# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="Logo used in Windows Vista, Windows Server 2008, Windows 7 among others"
HOMEPAGE="https://en.wikipedia.org/wiki/File:Windows_logo_-_2006.svg"
SRC_URI="https://upload.wikimedia.org/wikipedia/en/1/14/Windows_logo_-_2006.svg -> ${PN}-Windows_-_2006.svg
         https://upload.wikimedia.org/wikipedia/commons/5/5f/Windows_logo_-_2012.svg -> ${PN}-Windows_-_2012.svg
         https://upload.wikimedia.org/wikipedia/commons/d/da/Windows_logo_-_2002%E2%80%932012_%28Multicolored%29.svg -> ${PN}-Windows_-_2002-2012_(Multicolored).svg
         https://upload.wikimedia.org/wikipedia/commons/1/19/Windows_logo_-_2002%E2%80%932012_%28Black%29.svg -> ${PN}-Windows_-_2002-2012_(Black).svg
         https://upload.wikimedia.org/wikipedia/commons/6/6c/Windows_Phone_7.5_logo.svg -> ${PN}-Windows_Phone_7.5.svg
         https://upload.wikimedia.org/wikipedia/en/4/49/Mozilla_Mascot.svg -> ${PN}-Mozilla_Mascot.svg
         https://upload.wikimedia.org/wikipedia/commons/e/ee/Aperture_Science.svg -> ${PN}-Aperture_Science.svg
         https://upload.wikimedia.org/wikipedia/commons/1/1c/Haskell-Logo.svg -> ${PN}-Haskell.svg
         https://upload.wikimedia.org/wikipedia/commons/1/1b/Internet_Explorer_9_icon.svg -> ${PN}-Internet_Explorer_9.svg
         https://upload.wikimedia.org/wikipedia/commons/2/2f/Internet_Explorer_10_logo.svg -> ${PN}-Internet_Explorer_10.svg
         https://upload.wikimedia.org/wikipedia/commons/e/ed/Internet_Explorer_4_and_5_logo.svg -> ${PN}-Internet_Explorer_4_and_5.svg
         https://upload.wikimedia.org/wikipedia/commons/3/3a/Internet_Explorer_10_start_icon.svg -> ${PN}-Internet_Explorer_10_start.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d6/Microsoft_Edge_logo.svg -> ${PN}-Microsoft_Edge.svg
         https://upload.wikimedia.org/wikipedia/commons/4/46/Microsoft_FrontPage_logo.svg -> ${PN}-Microsoft_FrontPage.svg
         https://upload.wikimedia.org/wikipedia/commons/4/48/Outlook.com_icon.svg -> ${PN}-Outlook.com.svg
         https://upload.wikimedia.org/wikipedia/commons/0/05/Skype-for-Business.svg -> ${PN}-Skype-for-Business.svg
         https://upload.wikimedia.org/wikipedia/commons/e/e4/Sway_logo.svg -> ${PN}-Sway.svg
         https://upload.wikimedia.org/wikipedia/commons/3/3f/M_in_Blue.svg -> ${PN}-M_in_Blue.svg
         https://upload.wikimedia.org/wikipedia/commons/d/dd/Microsoft_Office_2013_logo.svg -> ${PN}-Microsoft_Office_2013.svg
         https://upload.wikimedia.org/wikipedia/commons/4/4f/Microsoft_Word_2013_logo.svg -> ${PN}-Microsoft_Word_2013.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b0/Microsoft_PowerPoint_2013_logo.svg -> ${PN}-Microsoft_PowerPoint_2013.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9e/Microsoft_OneNote_2013_logo.svg -> ${PN}-Microsoft_OneNote_2013.svg
         https://upload.wikimedia.org/wikipedia/commons/3/37/Microsoft_Access_2013_logo.svg -> ${PN}-Microsoft_Access_2013.svg
         https://upload.wikimedia.org/wikipedia/commons/8/86/Microsoft_Excel_2013_logo.svg -> ${PN}-Microsoft_Excel_2013.svg
         https://upload.wikimedia.org/wikipedia/commons/3/30/Microsoft_Lync_2013_logo.svg -> ${PN}-Microsoft_Lync_2013.svg
         https://upload.wikimedia.org/wikipedia/commons/0/0b/Microsoft_Outlook_2013_logo.svg -> ${PN}-Microsoft_Outlook_2013.svg
         https://upload.wikimedia.org/wikipedia/commons/e/e4/Visual_Studio_2013_Logo.svg -> ${PN}-Visual_Studio_2013.svg
         https://upload.wikimedia.org/wikipedia/commons/e/eb/Microsoft_Store_logo.svg -> ${PN}-Microsoft_Store.svg
         https://upload.wikimedia.org/wikipedia/commons/6/6e/Windows_Store.svg -> ${PN}-Windows_Store.svg
         https://upload.wikimedia.org/wikipedia/commons/9/99/Disqus_d_icon_%28blue%29.svg -> ${PN}-Disqus_d_(blue).svg
         https://upload.wikimedia.org/wikipedia/commons/a/ae/Disqus_d_icon_%28gray%29.svg -> ${PN}-Disqus_d_(gray).svg
         https://upload.wikimedia.org/wikipedia/commons/7/7b/Deutsche_Bank_logo_without_wordmark.svg -> ${PN}-Deutsche_Bank_without_wordmark.svg
         https://upload.wikimedia.org/wikipedia/commons/c/c2/F_icon.svg -> ${PN}-Facebook.svg
         https://upload.wikimedia.org/wikipedia/commons/8/8b/Twitter_logo_initial.svg -> ${PN}-Twitter_initial.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f9/Curation_tool_tag_icon.svg -> ${PN}-Curation_tool_tag.svg
         https://upload.wikimedia.org/wikipedia/commons/7/75/The_gesture.svg -> ${PN}-The_gesture.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9e/E17_enlightenment_logo_shiny_black_curved.svg -> ${PN}-E17_enlightenment_shiny_black_curved.svg
         https://upload.wikimedia.org/wikipedia/commons/6/6b/E17_enlightenment_logo_shiny_white_curved.svg -> ${PN}-E17_enlightenment_shiny_white_curved.svg
         https://upload.wikimedia.org/wikipedia/commons/4/4e/Electronic_cash_Logo-PIN-Pad.svg -> ${PN}-Electronic_cash-PIN-Pad.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9c/Golden_star.svg -> ${PN}-Golden_star.svg
         https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg -> ${PN}-Google_\"G\".svg
         https://upload.wikimedia.org/wikipedia/commons/9/9f/Google_plus_icon.svg -> ${PN}-Google_plus.svg
         https://upload.wikimedia.org/wikipedia/commons/2/2d/Google_Plus_logo_2015.svg -> ${PN}-Google_Plus_2015.svg
         https://upload.wikimedia.org/wikipedia/commons/4/45/New_Logo_Gmail.svg -> ${PN}-New_Gmail.svg
         https://upload.wikimedia.org/wikipedia/commons/1/11/X_from_Nexus_logo.svg -> ${PN}-X_from_Nexus.svg
         https://upload.wikimedia.org/wikipedia/commons/4/4f/Google_Photos_icon.svg -> ${PN}-Google_Photos.svg
         https://upload.wikimedia.org/wikipedia/commons/e/e8/Google_mic.svg -> ${PN}-Google_mic.svg
         https://upload.wikimedia.org/wikipedia/commons/1/1c/Google_Text_to_Speech_logo.svg -> ${PN}-Google_Text_to_Speech.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f8/Google_Camera_Icon.svg -> ${PN}-Google_Camera.svg
         https://upload.wikimedia.org/wikipedia/commons/8/83/Blogger_Shiny_Icon.svg -> ${PN}-Blogger_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b0/Diaspora_Shiny_Icon.svg -> ${PN}-Diaspora_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/c/c9/Linkedin.svg -> ${PN}-Linkedin.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f9/Linkedin_Shiny_Icon.svg -> ${PN}-Linkedin_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/2/25/HTML5_Shiny_Icon.svg -> ${PN}-HTML5_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/4/4e/Spotify_Shiny_Icon.svg -> ${PN}-Spotify_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f2/Pinterest_Shiny_Icon.svg -> ${PN}-Pinterest_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/0/02/Tumblr-2.svg -> ${PN}-Tumblr-2.svg
         https://upload.wikimedia.org/wikipedia/commons/2/2a/Vimeo_Shiny_Icon.svg -> ${PN}-Vimeo_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/6/68/Wordpress_Shiny_Icon.svg -> ${PN}-Wordpress_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d9/Rss-feed.svg -> ${PN}-Rss-feed.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b6/Rss_Shiny_Icon.svg -> ${PN}-Rss_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/2/23/Sharethis_Shiny_Icon.svg -> ${PN}-Sharethis_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/f/ff/Sharethis.svg -> ${PN}-Sharethis.svg
         https://upload.wikimedia.org/wikipedia/commons/4/44/Flickr.svg -> ${PN}-Flickr.svg
         https://upload.wikimedia.org/wikipedia/commons/2/26/Flickr_Shiny_Icon.svg -> ${PN}-Flickr_Shiny.svg
         https://upload.wikimedia.org/wikipedia/commons/3/31/Blogger.svg -> ${PN}-Blogger.svg
         https://upload.wikimedia.org/wikipedia/commons/d/da/Bluetooth.svg -> ${PN}-Bluetooth.svg
         https://upload.wikimedia.org/wikipedia/commons/2/28/Andantino_Opening.svg -> ${PN}-Andantino_Opening.svg
         https://upload.wikimedia.org/wikipedia/commons/3/36/Alquerque_board_at_starting_position.svg -> ${PN}-Alquerque_board_at_starting_position.svg
         https://upload.wikimedia.org/wikipedia/commons/1/13/Andantino_Game_Over.svg -> ${PN}-Andantino_Game_Over.svg
         https://upload.wikimedia.org/wikipedia/commons/1/1c/Chinese_checkers_start_positions.svg -> ${PN}-Chinese_checkers_start_positions.svg
         https://upload.wikimedia.org/wikipedia/commons/e/e5/Kensington_board.svg -> ${PN}-Kensington_board.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9e/NChart-Symbol_INT_Anchorage.svg -> ${PN}-NChart-Symbol_INT_Anchorage.svg
         https://upload.wikimedia.org/wikipedia/commons/1/15/NChart-Symbol_INT_Harbour_Master.svg -> ${PN}-NChart-Symbol_INT_Harbour_Master.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a2/NChart-Symbol_INT_Fishing_Harbour.svg -> ${PN}-NChart-Symbol_INT_Fishing_Harbour.svg
         https://upload.wikimedia.org/wikipedia/commons/3/3e/NChart-Symbol_INT_Marina.svg -> ${PN}-NChart-Symbol_INT_Marina.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9d/NChart-Symbol_INT_No_Anchorage_Area.svg -> ${PN}-NChart-Symbol_INT_No_Anchorage_Area.svg
         https://upload.wikimedia.org/wikipedia/commons/8/8b/NChart-Symbol_INT_No_Fishing_Area.svg -> ${PN}-NChart-Symbol_INT_No_Fishing_Area.svg
         https://upload.wikimedia.org/wikipedia/commons/c/c8/Love_Heart_symbol.svg -> ${PN}-Love_Heart_symbol.svg
         https://upload.wikimedia.org/wikipedia/commons/5/5e/Official_policy_seal_nt.svg -> ${PN}-Official_policy_seal_nt.svg
         https://upload.wikimedia.org/wikipedia/commons/8/8d/Portable_Radio.svg -> ${PN}-Portable_Radio.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d6/EnergieLabel.svg -> ${PN}-EnergieLabel.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a6/Sinnbild_elektrisch_betriebenes_Fahrzeug%2C_StVO_2015.svg -> ${PN}-Sinnbild_elektrisch_betriebenes_Fahrzeug,_StVO_2015.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b4/Sinnbild_Kfz.svg -> ${PN}-Sinnbild_Kfz.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f2/Age_warning_symbol.svg -> ${PN}-Age_warning_symbol.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d8/Ensiapu_715_tunnusosa.svg -> ${PN}-red-cross.svg
         https://upload.wikimedia.org/wikipedia/commons/8/8a/MUTCD_D9-21.svg -> ${PN}-MUTCD_D9-21.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a6/No_cellphone.svg -> ${PN}-No_cellphone.svg
         https://upload.wikimedia.org/wikipedia/commons/0/0e/ISO_7010_E003_-_First_aid_sign.svg -> ${PN}-ISO_7010_E003_-_First_aid_sign.svg
         https://upload.wikimedia.org/wikipedia/commons/6/6b/No_Smoking.svg -> ${PN}-No_Smoking.svg
         https://upload.wikimedia.org/wikipedia/commons/4/41/Underground.svg -> ${PN}-Underground.svg
         https://upload.wikimedia.org/wikipedia/commons/5/5b/Star_of_life2.svg -> ${PN}-Star_of_life2.svg
         https://upload.wikimedia.org/wikipedia/commons/7/7b/ISO_7010_P013.svg -> ${PN}-ISO_7010_P013.svg
         https://upload.wikimedia.org/wikipedia/commons/0/00/All_rights_reserved_white_logo.svg -> ${PN}-All_rights_reserved_white.svg
         https://upload.wikimedia.org/wikipedia/commons/b/bd/Cc-by_white.svg -> ${PN}-Cc-by_white.svg
         https://upload.wikimedia.org/wikipedia/commons/1/11/Cc-by_new_white.svg -> ${PN}-Cc-by_new_white.svg
         https://upload.wikimedia.org/wikipedia/commons/2/2f/Cc-nc_white.svg -> ${PN}-Cc-nc_white.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b3/Cc-nd_white.svg -> ${PN}-Cc-nd_white.svg
         https://upload.wikimedia.org/wikipedia/commons/d/df/Cc-sa_white.svg -> ${PN}-Cc-sa_white.svg
         https://upload.wikimedia.org/wikipedia/commons/5/53/Cc-white.svg -> ${PN}-Cc-white.svg
         https://upload.wikimedia.org/wikipedia/commons/4/44/Cc-zero_white.svg -> ${PN}-Cc-zero_white.svg
         https://upload.wikimedia.org/wikipedia/commons/5/55/Share_white.svg -> ${PN}-Share_white.svg
         https://upload.wikimedia.org/wikipedia/commons/5/51/Cc-pd-new_white.svg -> ${PN}-Cc-pd-new_white.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b0/Copyleft_white.svg -> ${PN}-Copyleft_white.svg
         https://upload.wikimedia.org/wikipedia/commons/3/38/Info_Simple.svg -> ${PN}-Info_Simple.svg
         https://upload.wikimedia.org/wikipedia/commons/c/cb/Ic_cloud_48px.svg -> ${PN}-Ic_cloud.svg
         https://upload.wikimedia.org/wikipedia/commons/2/22/Ic_cloud_queue_48px.svg -> ${PN}-Ic_cloud_queue.svg
         https://upload.wikimedia.org/wikipedia/commons/7/7a/Anarchy-symbol.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d8/Fxemoji_u1F4F5.svg -> ${PN}-Fxemoji_u1F4F5.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a0/Emojione_1F370.svg -> ${PN}-Emojione_1F370.svg
         https://upload.wikimedia.org/wikipedia/commons/9/92/Emojione_1F42A.svg -> ${PN}-Emojione_1F42A.svg
         https://upload.wikimedia.org/wikipedia/commons/0/0a/Emojione_1F604.svg -> ${PN}-Emojione_1F604.svg
         https://upload.wikimedia.org/wikipedia/commons/4/40/Emojione_1F64A.svg -> ${PN}-Emojione_1F64A.svg
         https://upload.wikimedia.org/wikipedia/commons/d/df/Emojione_2601.svg -> ${PN}-Emojione_2601.svg
         https://upload.wikimedia.org/wikipedia/commons/a/ac/Emojione_2611.svg -> ${PN}-Emojione_2611.svg
         https://upload.wikimedia.org/wikipedia/commons/0/08/Emojione_261D.svg -> ${PN}-Emojione_261D.svg
         https://upload.wikimedia.org/wikipedia/commons/c/c5/Emojione_2620.svg -> ${PN}-Emojione_2620.svg
         https://upload.wikimedia.org/wikipedia/commons/b/bd/Emojione_2666.svg -> ${PN}-Emojione_2666.svg
         https://upload.wikimedia.org/wikipedia/commons/0/08/Emojione_2668.svg -> ${PN}-Emojione_2668.svg
         https://upload.wikimedia.org/wikipedia/commons/e/e9/Emojione_26A1.svg -> ${PN}-Emojione_26A1.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f8/Emojione_270B.svg -> ${PN}-Emojione_270B.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d8/Emojione_270F.svg -> ${PN}-Emojione_270F.svg
         https://upload.wikimedia.org/wikipedia/commons/b/bb/Emojione_2764.svg -> ${PN}-Emojione_2764.svg
         https://upload.wikimedia.org/wikipedia/commons/6/66/Openlogo-debianV2.svg -> ${PN}-debian.svg
         https://upload.wikimedia.org/wikipedia/commons/4/4a/Debian-OpenLogo.svg -> ${PN}-debian-text.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9e/CentOS_Graphical_Symbol.svg -> ${PN}-centos.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a8/Former_UbuntuCoF.svg -> ${PN}-ubuntu-pre-2010.svg
         https://upload.wikimedia.org/wikipedia/commons/a/ab/Logo-ubuntu_cof-orange-hex.svg -> ${PN}-ubuntu.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a5/Archlinux-icon-crystal-64.svg -> ${PN}-Archlinux-icon-crystal.svg
         https://upload.wikimedia.org/wikipedia/commons/1/17/Archlinux-vert-dark.svg -> ${PN}-Archlinux-vert-dark
         https://upload.wikimedia.org/wikipedia/commons/2/21/Human-gnome-dev-zipdisk.svg -> ${PN}-ZIP-disk.svg
         https://upload.wikimedia.org/wikipedia/commons/8/83/Human-gnome-dev-jazdisk.svg -> ${PN}-JAZ-disk.svg"

LICENSE="fairuse"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~~mips ~ppc ~ppc64 ~s390 ~sh ~sparc x86 ~amd64-linux ~arm-linux ~ia64-linux ~x86-linux"

IUSE=""

# Need only a SVG-to-PNG converter and 'sed'
DEPEND="gnome-base/librsvg
        sys-apps/sed"
# No runtime dependencies
RDEPEND=""

S="${T}"

src_unpack() {
	einfo "Skipping unpacking"
}

src_compile() {
	TEMPFILE=$(mktemp --tmpdir=${T} ${PN}-XXXXXXXXX.png)
	for size in 128 16 192 22 24 256 32 36 48 512 64 72 96 ; do
		mkdir -p ${T}/${size} || die "Cannot create temporary directory '${T}/${size}'"
	done
	for svgfile in ${A} ; do \
		stem="$(basename "${svgfile}" | sed -e 's!^'"${PN}"'-!!;s/\.svg//;s/\([A-Z]\)/\L\1/g;s/[-_ (). ]//g')"
		for size in 128 16 192 22 24 256 32 36 48 512 64 72 96 ; do
			rsvg-convert -w ${size} -f png -o ${TEMPFILE} "${DISTDIR}/${svgfile}" || \
				die "Cannot create PNG file '${TEMPFILE}' from SVG file '${svgfile}'"
			height=$(identify -format '%h' ${TEMPFILE} || die "Cannot determine PNG file's size")
			if [ $height -gt $size ] ; then
				rm -f ${TEMPFILE}
				rsvg-convert -h ${size} -f png -o ${TEMPFILE} "${DISTDIR}/${svgfile}" || \
					die "Cannot create PNG file 'v' from SVG file '${svgfile}'"
			fi
			convert -gravity center -extent ${size}x${size} -background transparent ${TEMPFILE} ${T}/${size}/${stem}.png
		done
	done
}

src_install() {
	for size in 128 16 192 22 24 256 32 36 48 512 64 72 96 ; do
		insinto /usr/share/icons/hicolor/${size}x${size}/emblems
		doins ${T}/${size}/*.png || die "Cannot install PNG files of size ${size}"
	done
}
