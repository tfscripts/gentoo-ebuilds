# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="Logo used in Windows Vista, Windows Server 2008, Windows 7 among others"
HOMEPAGE="https://en.wikipedia.org/wiki/File:Windows_logo_-_2006.svg"
SRC_URI="https://upload.wikimedia.org/wikipedia/en/1/14/Windows_logo_-_2006.svg
         http://upload.wikimedia.org/wikipedia/commons/5/5f/Windows_logo_-_2012.svg
         http://upload.wikimedia.org/wikipedia/commons/d/da/Windows_logo_-_2002%E2%80%932012_%28Multicolored%29.svg
         http://upload.wikimedia.org/wikipedia/commons/1/19/Windows_logo_-_2002%E2%80%932012_%28Black%29.svg
         http://upload.wikimedia.org/wikipedia/commons/6/6c/Windows_Phone_7.5_logo.svg
         https://upload.wikimedia.org/wikipedia/en/4/49/Mozilla_Mascot.svg
         http://upload.wikimedia.org/wikipedia/commons/e/ee/Aperture_Science.svg
         http://upload.wikimedia.org/wikipedia/commons/1/1c/Haskell-Logo.svg
         http://upload.wikimedia.org/wikipedia/commons/1/1b/Internet_Explorer_9_icon.svg
         http://upload.wikimedia.org/wikipedia/commons/2/2f/Internet_Explorer_10_logo.svg
         http://upload.wikimedia.org/wikipedia/commons/e/ed/Internet_Explorer_4_and_5_logo.svg
         http://upload.wikimedia.org/wikipedia/commons/7/7b/Deutsche_Bank_logo_without_wordmark.svg
         http://upload.wikimedia.org/wikipedia/commons/3/3f/M_in_Blue.svg
         http://upload.wikimedia.org/wikipedia/commons/d/dd/Microsoft_Office_2013_logo.svg
         http://upload.wikimedia.org/wikipedia/commons/4/4f/Microsoft_Word_2013_logo.svg
         http://upload.wikimedia.org/wikipedia/commons/b/b0/Microsoft_PowerPoint_2013_logo.svg
         http://upload.wikimedia.org/wikipedia/commons/9/9e/Microsoft_OneNote_2013_logo.svg
         http://upload.wikimedia.org/wikipedia/commons/3/37/Microsoft_Access_2013_logo.svg
         http://upload.wikimedia.org/wikipedia/commons/8/86/Microsoft_Excel_2013_logo.svg
         http://upload.wikimedia.org/wikipedia/commons/3/30/Microsoft_Lync_2013_logo.svg
         http://upload.wikimedia.org/wikipedia/commons/9/99/Disqus_d_icon_%28blue%29.svg
         http://upload.wikimedia.org/wikipedia/commons/a/ae/Disqus_d_icon_%28gray%29.svg
         http://upload.wikimedia.org/wikipedia/commons/c/c2/F_icon.svg
         http://upload.wikimedia.org/wikipedia/commons/9/9f/Google_plus_icon.svg
         http://upload.wikimedia.org/wikipedia/commons/8/8b/Twitter_logo_initial.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d6/Microsoft_Edge_logo.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f9/Curation_tool_tag_icon.svg
         https://upload.wikimedia.org/wikipedia/commons/7/75/The_gesture.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9e/E17_enlightenment_logo_shiny_black_curved.svg
         https://upload.wikimedia.org/wikipedia/commons/6/6b/E17_enlightenment_logo_shiny_white_curved.svg
         https://upload.wikimedia.org/wikipedia/commons/4/4e/Electronic_cash_Logo-PIN-Pad.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9c/Golden_star.svg
         https://upload.wikimedia.org/wikipedia/commons/e/ed/Google_Plus_icon.svg
         https://upload.wikimedia.org/wikipedia/commons/7/7f/Google_Plus_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/8/83/Blogger_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b0/Diaspora_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f9/Linkedin_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/2/25/HTML5_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/4/4e/Spotify_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f2/Pinterest_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/0/02/Tumblr-2.svg
         https://upload.wikimedia.org/wikipedia/commons/e/e9/Linkedin_icon.svg
         https://upload.wikimedia.org/wikipedia/commons/c/c9/Linkedin.svg
         https://upload.wikimedia.org/wikipedia/commons/2/2a/Vimeo_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/6/68/Wordpress_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d9/Rss-feed.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b6/Rss_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/2/23/Sharethis_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/f/ff/Sharethis.svg
         https://upload.wikimedia.org/wikipedia/commons/4/44/Flickr.svg
         https://upload.wikimedia.org/wikipedia/commons/2/26/Flickr_Shiny_Icon.svg
         https://upload.wikimedia.org/wikipedia/commons/3/31/Blogger.svg
         https://upload.wikimedia.org/wikipedia/commons/d/da/Bluetooth.svg
         https://upload.wikimedia.org/wikipedia/commons/2/28/Andantino_Opening.svg
         https://upload.wikimedia.org/wikipedia/commons/3/36/Alquerque_board_at_starting_position.svg
         https://upload.wikimedia.org/wikipedia/commons/1/13/Andantino_Game_Over.svg
         https://upload.wikimedia.org/wikipedia/commons/1/1c/Chinese_checkers_start_positions.svg
         https://upload.wikimedia.org/wikipedia/commons/e/e5/Kensington_board.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a0/Emojione_1F370.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9e/NChart-Symbol_INT_Anchorage.svg
         https://upload.wikimedia.org/wikipedia/commons/1/15/NChart-Symbol_INT_Harbour_Master.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a2/NChart-Symbol_INT_Fishing_Harbour.svg
         https://upload.wikimedia.org/wikipedia/commons/3/3e/NChart-Symbol_INT_Marina.svg
         https://upload.wikimedia.org/wikipedia/commons/9/9d/NChart-Symbol_INT_No_Anchorage_Area.svg
         https://upload.wikimedia.org/wikipedia/commons/8/8b/NChart-Symbol_INT_No_Fishing_Area.svg
         https://upload.wikimedia.org/wikipedia/commons/c/c8/Love_Heart_symbol.svg
         https://upload.wikimedia.org/wikipedia/commons/5/5e/Official_policy_seal_nt.svg
         https://upload.wikimedia.org/wikipedia/commons/8/8d/Portable_Radio.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d6/EnergieLabel.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a6/Sinnbild_elektrisch_betriebenes_Fahrzeug%2C_StVO_2015.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b4/Sinnbild_Kfz.svg
         https://upload.wikimedia.org/wikipedia/commons/f/f2/Age_warning_symbol.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d8/Ensiapu_715_tunnusosa.svg
         https://upload.wikimedia.org/wikipedia/commons/8/8a/MUTCD_D9-21.svg
         https://upload.wikimedia.org/wikipedia/commons/a/a6/No_cellphone.svg
         https://upload.wikimedia.org/wikipedia/commons/6/6b/No_Smoking.svg
         https://upload.wikimedia.org/wikipedia/commons/4/41/Underground.svg
         https://upload.wikimedia.org/wikipedia/commons/5/5b/Star_of_life2.svg
         https://upload.wikimedia.org/wikipedia/commons/d/d8/Fxemoji_u1F4F5.svg
         https://upload.wikimedia.org/wikipedia/commons/7/7b/ISO_7010_P013.svg
         https://upload.wikimedia.org/wikipedia/commons/0/00/All_rights_reserved_white_logo.svg
         https://upload.wikimedia.org/wikipedia/commons/b/bd/Cc-by_white.svg
         https://upload.wikimedia.org/wikipedia/commons/1/11/Cc-by_new_white.svg
         https://upload.wikimedia.org/wikipedia/commons/2/2f/Cc-nc_white.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b3/Cc-nd_white.svg
         https://upload.wikimedia.org/wikipedia/commons/d/df/Cc-sa_white.svg
         https://upload.wikimedia.org/wikipedia/commons/5/53/Cc-white.svg
         https://upload.wikimedia.org/wikipedia/commons/4/44/Cc-zero_white.svg
         https://upload.wikimedia.org/wikipedia/commons/5/55/Share_white.svg
         https://upload.wikimedia.org/wikipedia/commons/5/51/Cc-pd-new_white.svg
         https://upload.wikimedia.org/wikipedia/commons/b/b0/Copyleft_white.svg
         https://upload.wikimedia.org/wikipedia/commons/3/38/Info_Simple.svg
         https://upload.wikimedia.org/wikipedia/commons/c/cb/Ic_cloud_48px.svg
         https://upload.wikimedia.org/wikipedia/commons/2/22/Ic_cloud_queue_48px.svg
         https://upload.wikimedia.org/wikipedia/commons/d/df/Emojione_2601.svg"

LICENSE="fairuse"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~~mips ~ppc ~ppc64 ~s390 ~sh ~sparc x86 ~amd64-linux ~arm-linux ~ia64-linux ~x86-linux"

IUSE=""

# Need only a SVG-to-PNG converter and 'sed'
DEPEND="gnome-base/librsvg
        sys-apps/sed"
# No runtime dependencies
RDEPEND=""

S="${T}"

src_compile() {
	TEMPFILE=$(mktemp ${T}/XXXXXXXXX.png)
	for size in 128 16 192 22 24 256 32 36 48 512 64 72 96 ; do
		mkdir -p ${T}/${size} || die "Cannot create temporary directory '${T}/${size}'"
	done
	for svgfile in ${A} ; do \
		stem="$(basename "${svgfile}" | sed -e 's/\.svg//;s/%21/!/g;s/%23/#/g;s/%24/$/g;s/%26/&/g;s/%27/'"'"'/g;s/%28/(/g;s/%29/)/g;s/\([A-Z]\)/\L\1/g;s/[-_ (). ]//g;s/logo//g;s/icon//g;s/%e2%80%93/-/gi')"
		for size in 128 16 192 22 24 256 32 36 48 512 64 72 96 ; do
			rsvg-convert -w ${size} -f png -o ${TEMPFILE} "${DISTDIR}/${svgfile}" || \
				die "Cannot create PNG file '${TEMPFILE}' from SVG file '${svgfile}'"
			height=$(identify -format '%h' ${TEMPFILE} || die "Cannot determine PNG file's size")
			if [ $height -gt $size ] ; then
				rm -f ${TEMPFILE}
				rsvg-convert -h ${size} -f png -o ${TEMPFILE} "${DISTDIR}/${svgfile}" || \
					die "Cannot create PNG file 'v' from SVG file '${svgfile}'"
			fi
			convert -gravity center -extent ${size}x${size} -background transparent ${TEMPFILE} ${T}/${size}/${stem}.png
		done
	done
}

src_install() {
	for size in 128 16 192 22 24 256 32 36 48 512 64 72 96 ; do
		insinto /usr/share/icons/hicolor/${size}x${size}/emblems
		doins ${T}/${size}/*.png || die "Cannot install PNG files of size ${size}"
	done
}
