This repository contains a collection of ebuild files for
Gentoo Linux and derived Linux distributions.

The ebuild files state the license under which they are
published in their first lines each.