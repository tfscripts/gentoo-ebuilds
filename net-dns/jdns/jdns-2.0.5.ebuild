# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit cmake-utils

# Short one-line description of this package.
DESCRIPTION="A simple DNS implementation that can perform normal DNS queries"

# Homepage, not used by Portage directly but handy for developer reference
HOMEPAGE="https://github.com/psi-im/jdns"

# Point to any required sources; these will be automatically downloaded by
# Portage.
SRC_URI="https://github.com/psi-im/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="qt5"

DEPEND="qt5? (
               dev-qt/qtcore:5
               dev-qt/qtnetwork:5
             )"
RDEPEND="${DEPEND}"

src_configure() {
	local mycmakeargs=(
		-DBUILD_QJDNS="$(usex qt5)"
	)
	cmake-utils_src_configure
}
