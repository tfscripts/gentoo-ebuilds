# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils unpacker xdg

DESCRIPTION="Cross-platform, encrypted instant messaging client"
HOMEPAGE="https://wire.com https://github.com/wireapp/wire-desktop"
SRC_URI="https://github.com/wireapp/wire-desktop/releases/download/linux%2F${PV}/Wire-${PV}_amd64.deb"

RESTRICT="mirror"
LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

# TODO: verify that list of RDEPEND is complete
RDEPEND="media-libs/alsa-lib
	gnome-base/gconf
	x11-libs/gtk+:2
	x11-libs/libXtst
	dev-libs/nss"

DEPEND="${RDEPEND}"

RESTRICT="strip"
S=${WORKDIR}

src_unpack() {
	# Unpack Debian package containing application's files
	unpack_deb ${A}

	# Rename /opt/Wire to /opt/${PN}
	mv ${S}/opt/Wire ${S}/opt/${PN}
}

src_configure() {
	# Fix issues with shipped .desktop file:
	# - Beautify name to 'Wire Desktop'
	# - Set comment to same as used in this ebuild's DESCRIPTION variable
	# - Change path from /opt/Wire to /opt/${PN}
	sed -i -e 's/^Name=.*/Name=Wire Desktop/;s/^Comment=.*/Comment='"${DESCRIPTION}"'/;s!/opt/Wire/!/opt/'"${PN}"'/!g' ${S}/usr/share/applications/wire-desktop.desktop
}

src_install() {
	cd ${S}

	# Do not install ChangeLog
	rm -rf usr/share/doc/wire-desktop/

	insinto /opt
	doins -r opt/${PN}
	insinto /usr
	doins -r usr/share
	fperms 755 /opt/${PN}/wire-desktop
}
