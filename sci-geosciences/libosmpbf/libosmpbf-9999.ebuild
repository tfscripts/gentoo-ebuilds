# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

MY_PN=OSM-binary

inherit cmake-utils

DESCRIPTION="Osmpbf is a library to read and write OpenStreetMap PBF files"
HOMEPAGE="https://github.com/openstreetmap/OSM-binary"

if [[ ${PV} = 9999 ]]; then
	inherit git-r3
	SRC_URI=""
	KEYWORDS=""
	EGIT_REPO_URI="https://github.com/openstreetmap/OSM-binary.git"
else
	SRC_URI="https://github.com/openstreetmap/OSM-binary/archive/refs/tags/v1.5.0.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~x86 ~amd64"
	S=${WORKDIR}/${MY_PN}-${PV}
fi
	

LICENSE="GPL-3"
SLOT="0"
IUSE=""

DEPEND="dev-libs/protobuf"
RDEPEND="${DEPEND}"
