# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils

DESCRIPTION="When you need a web server in a hurry"
HOMEPAGE="https://unix4lyfe.org/darkhttpd/"
SRC_URI="https://unix4lyfe.org/${PN}/${P}.tar.bz2"
LICENSE="BSD-1"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

src_install() {
	# Provide custom install function as this software itself doesn't provide one

	dobin darkhttpd
	
	insinto /lib/systemd/system
	doins ${FILESDIR}/darkhttpd.service
	doins ${FILESDIR}/darkhttpd@.service
	
	dodir /etc/conf.d
	touch ${D}/etc/conf.d/mimetypes
}
