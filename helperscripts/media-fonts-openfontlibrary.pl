use strict;
use warnings;

use LWP::UserAgent;
use File::Temp qw/ tempdir /;
use Archive::Extract;
use File::Find;

my $ua = LWP::UserAgent->new(
    agent      => "media-fonts-openfontlibrary.pl",
    keep_alive => 1,
    env_proxy  => 1,
);

sub downloadordie {
    my $url        = shift(@_);
    my $outputfile = shift(@_);
    print STDERR "Fetching \"$url\"\n";
    my $res = $ua->simple_request( HTTP::Request->new( GET => $url ) );
    if ( $res->code == 200 ) {
        if ( defined($outputfile) ) {
            print STDERR "Saving to \"$outputfile\"\n";
            open( FILE, ">$outputfile" )
              || die "Can't open \"$outputfile\": $!\n";
            binmode FILE;
            print FILE $res->content;
            close FILE;
            return $res->code;
        }
        else {
            return $res->content;
        }
    }
    else {
        print "\n";
        die "Cannot download \"$url\", got code " . $res->code;
    }
}

my %examplefontname = ();

sub locatefontfiles {
    my $name = $File::Find::name;
    if ( $name =~ /[.]([a-z]{2,4})$/i ) {
        $examplefontname{$1} = $name;
    }
}

my @categories = (
    "blackletter", "dingbat",    "display", "handwriting",
    "monospaced",  "sans-serif", "serif"
);

# my @categories = ("blackletter");
my @fontnameextensions = ( "otf", "pfb", "ttf" );
my %fonturls = ();

for my $category (@categories) {
    my $categoryurl =
      "http://openfontlibrary.org/en/search?category=" . $category;
    my $html = downloadordie($categoryurl);

    while ( $html =~ m!(http://openfontlibrary.org/en/font/[-_a-z0-9]+)!gi ) {
        $fonturls{$1}{"category"} = $category;
        $fonturls{$1}{"category"} =~ s/-//g;
    }
}

my $maxdate = 0;
for my $fonturl ( sort keys %fonturls ) {
    if ( $fonturl =~ m!ti[nm]esgothic!i ) {
        print STDERR "Skipping blacklisted font " . $_. "\n";
        next;
    }

    my $html = downloadordie($fonturl);
    ( my $licensetext ) =
      $html =~ m!<dt>License</dt><dd><a href="[^"]+">([^<]+)<!;
    if ( !defined($licensetext) ) {
        print STDERR "Cannot determine license for this font, skipping\n";
        next;
    }
    my $license = undef;
    if ( $licensetext eq "OFL (SIL Open Font License)" ) {
        $license = "ofl";
    }
    elsif ( $licensetext eq "CC-BY" ) {
        $license = "CCPL-Attribution-3.0";
    }
    elsif ( $licensetext eq "CC-BY-SA" ) {
        $license = "CCPL-Attribution-ShareAlike-3.0";
    }
    elsif ( $licensetext eq "GNU General Public License" ) {
        $license = "GPL-3";
    }
    elsif ( $licensetext eq "GNU Lesser General Public License" ) {
        $license = "LGPL-3";
    }
    elsif ( $licensetext eq "MIT (X11) License" ) {
        $license = "MIT";
    }
    elsif ( $licensetext eq "Public Domain (not a license)" ) {
        $license = "public-domain";
    }
    elsif ( $licensetext eq "Apache 2.0" ) {
        $license = "Apache-2.0";
    }
    else {
        die "Unknown license text \"$licensetext\"";
    }
    $fonturls{$fonturl}{"license"} = $license;

    ( my $year, my $month, my $day ) =
      $html =~ m!<dt>Updated</dt><dd>(20\d{2})-(\d{2})-(\d{2})<!;
    my $date = scalar( $year . $month . $day );
    if ( $date > $maxdate ) { $maxdate = $date; }

    ( my $downloadpath ) = $html =~ m!<p class="download">[^<]*<a [^>]*href="(/assets/downloads/[-_a-z]+/[0-9a-f]+/[-_a-z.]+)"!;
    my $downloadurl = "http://openfontlibrary.org" . $downloadpath;
    $fonturls{$fonturl}{"url"} = $downloadurl;

    my $tmpdir = tempdir( CLEANUP => 1 );
    my $tmpzip = $tmpdir . "/font.zip";
    downloadordie( $downloadurl, $tmpzip );

    my $archive = Archive::Extract->new( archive => $tmpzip );
    $archive->extract( to => $tmpdir ) || die "Archive extraction failed";

    %examplefontname = ();
    find( \&locatefontfiles, ($tmpdir) );

    my $fontpattern = undef;
    for my $fontnameextension (@fontnameextensions) {
        if ( defined( $examplefontname{$fontnameextension} ) ) {
            $fontpattern = $examplefontname{$fontnameextension};
            $fontpattern =~ s!$tmpdir/!!;
            $fontpattern =~ s![^/]+/!*/!g;
            $fontpattern =~ s![^/]+(\.[a-z]{2,4})$!*$1!g;
            last;
        }
    }
    if ( !defined($fontpattern) ) {
        die "No known font pattern found in archive";
    }
    $fonturls{$fonturl}{"pattern"} = $fontpattern;

    print "          "
      . $fonturls{$fonturl}{"category"} . "|"
      . $fonturls{$fonturl}{"url"} . "|"
      . $fonturls{$fonturl}{"pattern"} . "|"
      . $fonturls{$fonturl}{"license"} . "\n";
}

print STDERR "Rename output to \"media-fonts/openfontlibrary-".$maxdate.".ebuild\"\n";
