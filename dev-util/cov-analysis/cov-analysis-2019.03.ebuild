# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils

DESCRIPTION="Coverity Scan Build Tool for C/C++"
HOMEPAGE="https://scan.coverity.com/"
# https://scan.coverity.com/download/cxx/linux
# https://scan.coverity.com/download/cxx/linux64
SRC_URI="${PN}-linux64-${PV}.tar.gz"
LICENSE=""

SLOT="0"
KEYWORDS="-* ~amd64"
IUSE=""
RESTRICT="fetch strip"

DEPEND=""
RDEPEND="${DEPEND}"

S=${WORKDIR}/${PN}-linux64-${PV}

pkg_nofetch() {
	einfo "To download the Coverity Scan tool, please sign in at"
	einfo "   https://scan.coverity.com/"
	einfo "and then download the build tool by following this link:"
	einfo "   https://scan.coverity.com/download/cxx/linux64"
}

src_unpack() {
	# Synopsis made the mistake of compressing cov-analysis-linux64-2019.03.tar.gz
	# *twice* with gzip
	if [ ${PV} = "2019.03" ] ; then
		gunzip <"${DISTDIR}/${A}" >"${T}/${PN}-linux64-${PV}.tar.gz" || die "Removing first layer of compression failed"
		unpack "${T}/${PN}-linux64-${PV}.tar.gz" || die "Failed to unpack '${T}/${PN}-linux64-${PV}.tar.gz'"
	else
		eutils_src_unpack
	fi
}

src_install() {
	dodir /opt
	cp -r "${S}" "${D}/opt/" || die "Could not copy files to '${D}/opt'"

	local pathdirectory
	pathdirectory="/opt/${PN}-linux64-${PV}/bin"

	dodir /etc/profile.d
	echo 'export PATH="${PATH}:'"${pathdirectory}"'"' >${D}/etc/profile.d/${PN}.sh
	echo 'setenv PATH "${PATH}:'"${pathdirectory}"'"' >${D}/etc/profile.d/${PN}.csh
	chmod 755 ${D}/etc/profile.d/${PN}.sh ${D}/etc/profile.d/${PN}.csh
}
