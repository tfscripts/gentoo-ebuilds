# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7,8,9,10} )
PYTHON_REQ_USE="tk"
inherit distutils-r1 desktop

DESCRIPTION="A Python IDE for beginners"
HOMEPAGE="https://thonny.org/"
SRC_URI="https://github.com/thonny/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
RESTRICT="mirror"
LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

DEPEND="${PYTHON_DEPS}
>=dev-python/send2trash-1.4[${PYTHON_USEDEP}]
>=dev-python/mypy-0.670[${PYTHON_USEDEP}]
>=dev-python/pylint-1.9[${PYTHON_USEDEP}]
>=dev-python/docutils-0.14[${PYTHON_USEDEP}]
>=dev-python/mypy-0.670[${PYTHON_USEDEP}]
>=dev-python/pyserial-3.4[${PYTHON_USEDEP}]
>=dev-python/jedi-0.13[${PYTHON_USEDEP}]
>=dev-python/asttokens-1.1[${PYTHON_USEDEP}]"

RDEPEND="${DEPEND}"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

src_install() {
	distutils-r1_src_install

	for s in 16 22 32 48 64 128 192 256 ; do \
		newicon -s ${s} packaging/icons/thonny-${s}x${s}.png thonny.png || die "Cannot install icon if size ${s}x${s}" ; \
	done

	make_desktop_entry /usr/bin/thonny Thonny thonny "Development;IDE"
}
