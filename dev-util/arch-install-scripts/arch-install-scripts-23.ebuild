# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Arch Linux install tools (pacstrap, genfstab, arch-chroot)"
HOMEPAGE="https://git.archlinux.org/arch-install-scripts.git https://www.archlinux.org/packages/?name=arch-install-scripts"
EGIT_REPO_URI="https://projects.archlinux.org/arch-install-scripts.git"
EGIT_COMMIT="v${PV}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="sys-apps/pacman
         sys-apps/coreutils
         sys-apps/util-linux
         virtual/awk
         app-shells/bash"

src_install() {
	emake DESTDIR="${D}" PREFIX=/usr install
}
