# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Tool to assemble files for the Ravensburger TipToi pen"

HOMEPAGE="https://github.com/entropia/tip-toi-reveng
          http://tttool.entropia.de/"

MY_PN=${PN/-bin}
MY_P=${P/-bin}
SRC_URI="https://github.com/entropia/tip-toi-reveng/releases/download/${PV}/${MY_P}.zip"
S="${WORKDIR}/${MY_P}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="app-accessibility/espeak"

src_compile() {
	einfo "Binary package does not need to be compiled"
}

src_install() {
	dobin linux/tttool
	dodoc README.md Changelog.md oid-decoder.html
}
